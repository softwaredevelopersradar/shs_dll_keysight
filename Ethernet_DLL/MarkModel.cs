﻿namespace Ethernet_DLL
{
    public class MarkModel
    {
        public string XMARKData { get; set; }

        public string YMARKData { get; set; }

        public MarkModel(string xMARKData, string yMARKData)
        {
            XMARKData = xMARKData;
            YMARKData = yMARKData;
        }

    }
}
