﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для FreqTableControl.xaml
    /// </summary>
    public partial class FreqTableControl : UserControl
    {
        public FreqTableControl()
        {
            InitializeComponent();
        }

        private int myId = 1;

        private void WindowChange_onChange(object sender, TableFreq e)
        {
            int ind = 0;
            if (FreqTable.SelectedIndex > -1)
            {
                ind = FreqTable.SelectedIndex;
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
            }
            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = (TableFreq.hid)(byte)e.Hindrance, Lit = 2, Power = e.Power });
        }

        AddWindow windowAdd;
        public static event EventHandler<TableFreq> Change;

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            windowAdd = new AddWindow();
            windowAdd.OnAdd += Window_onAdd;
            windowAdd.ShowDialog();
        }

        private void Window_onAdd(object sender, TableFreq e)
        {
            if (e.Hindrance == TableFreq.hid.FirstParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = e.Lit, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power });
            }
            else if (e.Hindrance == TableFreq.hid.SecondParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = e.Lit, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power });
            }
            else
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = e.Lit, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
            }
            myId++;
            windowAdd.Close();
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
                myId--;
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FreqTable.Items.Clear();
            myId = 1;
        }

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                ChangeWindow windowChange = new ChangeWindow();
                windowChange.onChange += WindowChange_onChange;
                windowChange.Show();
                var a = (TableFreq)FreqTable.SelectedItem;
                Change?.Invoke(this, (TableFreq)FreqTable.SelectedItem);
            }
        }
    }
}
