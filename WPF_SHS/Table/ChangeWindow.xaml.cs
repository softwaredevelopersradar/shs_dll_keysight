﻿using ControlAddToTable;
using System;
using System.Windows;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для ChangeWindow.xaml
    /// </summary>
    public partial class ChangeWindow : Window
    {
        public event EventHandler<TableFreq> onChange;

        public ChangeWindow()
        {
            InitializeComponent();
            GrozaS.Change += Control_Change;
            GrozaZ1.Change += Control_Change;
            Control.OnButton += Control_onButton;
        }

        private void Control_onButton(object sender, ModelControl e)
        {
            var a = (byte)e.InterferenceParam;
            TableFreq table = new TableFreq();
            table.Id = 1;
            table.FreqKHz = e.FKHz;
            table.DFreqKHz = e.DFKHz;
            table.Hindrance = (TableFreq.hid)a;
            table.Dev = e.Dev;
            table.Man = e.Man;
            table.Lit = 0;
            table.Power = e.Power;
            onChange?.Invoke(this, table);
        }

        private void Control_Change(object sender, TableFreq e)
        {
            if (e.Hindrance == TableFreq.hid.FirstParam)
            {
                ModelControl model = new ModelControl() { FKHz = e.FreqKHz, DFKHz = e.DFreqKHz, InterferenceParam = (InterferenceP)(byte)e.Hindrance, Dev = 0, Man = 0, SpSc = 0, Power = e.Power };
                Control.OnChange(model);
            }
            else if (e.Hindrance == TableFreq.hid.SecondParam)
            {
                ModelControl model = new ModelControl() { FKHz = e.FreqKHz, DFKHz = e.DFreqKHz, InterferenceParam = (InterferenceP)(byte)e.Hindrance, Dev = 0, Man = e.Man, SpSc = 0, Power = e.Power };
                Control.OnChange(model);
            }
            else if (e.Hindrance == TableFreq.hid.ThirdParam || e.Hindrance == TableFreq.hid.FourthParam)
            {
                ModelControl model = new ModelControl() { FKHz = e.FreqKHz, DFKHz = e.DFreqKHz, InterferenceParam = (InterferenceP)(byte)e.Hindrance, Dev = e.Dev, Man = 0, SpSc = e.Man, Power = e.Power };
                Control.OnChange(model);
            }
        }

    }
}
