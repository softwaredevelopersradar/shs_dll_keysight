﻿using System;
using System.Windows;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для AddWindowF.xaml
    /// </summary>
    public partial class AddWindowF : Window
    {
        public AddWindowF()
        {
            InitializeComponent();
            Control.OnButton += Control_onButton;
        }

        private void Control_onButton(object sender, ControlAddToTableF.ModelControl e)
        {
            FTable table = new FTable
            {
                Id = 0,
                Register = e.Register,
                FStart = e.FStart,
                FStop = e.FStop,
                KStart = e.KStart,
                KStop = e.KStop
            };
            OnAdd?.Invoke(this, table);
        }

        public event EventHandler<FTable> OnAdd;
    }
}
