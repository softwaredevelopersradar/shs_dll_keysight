﻿using ControlAddToTableF;
using System;
using System.Windows;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для ChangeWindowF.xaml
    /// </summary>
    public partial class ChangeWindowF : Window
    {
        public ChangeWindowF()
        {
            InitializeComponent();
            GrozaF.Change += Control_Change;
            Control.OnButton += Control_onButton;
        }

        public event EventHandler<FTable> onChange;

        private void Control_onButton(object sender, ModelControl e)
        {
            FTable table = new FTable();
            table.Register = e.Register;
            table.FStart = e.FStart;
            table.FStop = e.FStop;
            table.KStart = e.KStart;
            table.KStop = e.KStop;
            onChange?.Invoke(this, table);
        }

        private void Control_Change(object sender, FTable e)
        {
            ModelControl model = new ModelControl() { Register = e.Register, FStart = e.FStart, FStop = e.FStop, KStart = e.KStart, KStop = e.KStop };
            Control.OnChange(model);
        }
    }
}
