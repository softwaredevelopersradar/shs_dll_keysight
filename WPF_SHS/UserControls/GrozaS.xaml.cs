﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для GrozaS.xaml
    /// </summary>
    public partial class GrozaS : UserControl
    {
        public GrozaS()
        {
            InitializeComponent();
            Dump();
        }

        #region Dump
        private void Dump()
        {
            MainWindow.OnAddStatusToSecondTable += MainWindow_OnAddStatusToSecondTable;
            AddEmptyRowsToSecondTable();
            FreqTable.IsReadOnly = true;
            SFreqTable.IsReadOnly = true;
            NumericPower.Minimum = 0;
            NumericPower.Maximum = 100;
            NumericPower.Value = 0;
            SpoofNumeric.Minimum = 0;
            SpoofNumeric.Maximum = 15;
            SpoofNumeric.Value = 0;

            for (int i = 1; i < 8; i++)
            {
                SectorNumberBoxByteSix.Items.Add(i);
                SectorNumberBoxByteSeven.Items.Add(i);
            }

            SectorNumberBoxByteSeven.SelectedIndex = 0;
            SectorNumberBoxByteSix.SelectedIndex = 0;

            for (int i = 0; i < 4; i++)
            {
                LitComboBox.Items.Add(i);
            }
            LitComboBox.SelectedIndex = 0;
            ReadConfig();
        }
        #endregion

        #region Variables
        AddWindowS windowAdd;
        ChangeWindowS windowChange;
        XmlSerializer formatter = new XmlSerializer(typeof(TableFreq[]));
        private int myId = 1;
        public bool[] SLit = new bool[] { false, false, false };
        private string[] RangeLit = new string[] { "100-500", "500-2500", "2500-6000", "GNSS", "SPOOF" };
        #endregion

        #region Event`s
        public event EventHandler<LetterEventArgs> OnStatus;
        public event EventHandler<LetterEventArgs> OnStatusAntenna;
        public event EventHandler<SetNaViEventArgs> OnSetNaVi;
        public event EventHandler<SetGNSSEventArgs> OnSetGNSS;
        public event EventHandler<SetSpoofEventArgs> OnSpoof;
        public event EventHandler<LetterEventArgs> OnOffRad;
        public event EventHandler<LetterEventArgs> OnReset;
        public event EventHandler<LetterEventArgs> OnRelaySwitching;
        public event EventHandler<ParamEventArgs> OnParam;
        public event EventHandler<SwitchEventArgs> OnSwitch;
        public static event EventHandler<TableFreq> Change;
        #endregion

        #region Button`s
        private void AddEmptyRowsToSecondTable()
        {
            for (int i = 0; i < 5; i++)
            {
                SFreqTable.Items.Add(new STableFreq() { Lite = (i + 1).ToString(), LitDescrip = RangeLit[i], Degree = string.Empty, Amperage = string.Empty, Emitting = false, Error = true, Power = false, Signal = false });
            }
        }

        private void MainWindow_OnAddStatusToSecondTable(object sender, ConfirmStatusEventArgs e)
        {
            try
            {
                int count = 1;
                Dispatcher.Invoke(() => { SFreqTable.Items.Clear(); });
                CheckGrozaSToImgSourceConverter.i = 0;
                foreach (var i in e.ParamAmp)
                {
                    Dispatcher.Invoke(() => { SFreqTable.Items.Add(new STableFreq() { Lite = count.ToString(), LitDescrip = RangeLit[count - 1], Signal = Convert.ToBoolean(i.Snt), Emitting = Convert.ToBoolean(i.Rad), Degree = i.Temp.ToString(), Amperage = (i.Current / 10).ToString(), Power = Convert.ToBoolean(i.Power), Error = Convert.ToBoolean(i.Error) }); });
                    count++;
                }
            }
            catch { }
        }

        private void NaViBox_Checked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = true;
            GPS_L2.IsChecked = true;
            Glonass_L1.IsChecked = true;
            Glonass_L2.IsChecked = true;
            GNSS_L1.IsChecked = true;
            GNSS_L2.IsChecked = true;
        }

        private void NaViBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = false;
            GPS_L2.IsChecked = false;
            Glonass_L1.IsChecked = false;
            Glonass_L2.IsChecked = false;
            GNSS_L1.IsChecked = false;
            GNSS_L2.IsChecked = false;
        }

        private void SetReceivingSwitch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSwitch?.Invoke(this, new SwitchEventArgs(new byte[] { Convert.ToByte(SectorNumberBoxByteSix.Text), Convert.ToByte(SectorNumberBoxByteSeven.Text) }, true));
            }
            catch { }
        }

        private void SetTransmissionSwitch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSwitch?.Invoke(this, new SwitchEventArgs(new byte[] { Convert.ToByte(SectorNumberBoxByteSix.Text), Convert.ToByte(SectorNumberBoxByteSeven.Text) }, false));
            }
            catch { }
        }

        private void StatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnStatus?.Invoke(this, new LetterEventArgs(0));
            }
            catch { }
        }

        private void StatusAntennaButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnStatusAntenna?.Invoke(this, new LetterEventArgs(0));
            }
            catch { }
        }

        private void SetNaViButton_Click(object sender, RoutedEventArgs e)
        {
            OnSetNaVi?.Invoke(this, new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked, (bool)PowerButton.IsChecked, (byte)NumericPower.Value));
        }

        private void GNSSButton_Click(object sender, RoutedEventArgs e)
        {
            OnSetGNSS?.Invoke(this, new SetGNSSEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked, (bool)GNSS_L1.IsChecked, (bool)GNSS_L2.IsChecked, (bool)GNSS_L1.IsChecked, (bool)GNSS_L2.IsChecked, (bool)PowerButton.IsChecked, (byte)NumericPower.Value));
        }

        private void ParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] bytes = new byte[5];
                int x = FreqTable.SelectedIndex;
                List<TableFreq> tables = new List<TableFreq>();
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    tables.Add(item);
                    bytes[i] = item.Power;
                }
                FreqTable.SelectedIndex = x;
                OnParam?.Invoke(this, new ParamEventArgs(tables, (bool)PowerButton.IsChecked, bytes));
            }
            catch
            {
                MessageBox.Show("Выберите значение!");
            }
        }

        private void SpuffingButton_Click(object sender, RoutedEventArgs e)
        {
            OnSpoof?.Invoke(this, new SetSpoofEventArgs((bool)PowerButton.IsChecked, (byte)SpoofNumeric.Value));
        }

        private void OffRadButton_Click(object sender, RoutedEventArgs e)
        {
            OnOffRad?.Invoke(this, new LetterEventArgs(Convert.ToByte(LitComboBox.Text)));
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            OnReset?.Invoke(this, new LetterEventArgs(0));
        }

        private void SwapReleButton_Click(object sender, RoutedEventArgs e)
        {
            byte letter = new byte();
            if ((bool)ReleToogle.IsChecked)
            {
                letter = 0;
            }
            else
            {
                letter = 1;
            }
            OnRelaySwitching?.Invoke(this, new LetterEventArgs(letter));
        }
        #endregion

        #region FreqTable
        public List<TableFreq> GetTableS()
        {
            int x = FreqTable.SelectedIndex;
            List<TableFreq> tables = new List<TableFreq>();
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                tables.Add(item);
            }
            FreqTable.SelectedIndex = x;
            return tables;
        }

        private void WindowChange_onChange(object sender, TableFreq e)
        {
            try
            {
                int ind = 0;
                if (FreqTable.SelectedIndex > -1)
                {
                    int L = 0;
                    string Descrip = string.Empty;
                    ind = FreqTable.SelectedIndex;
                    var item = (TableFreq)FreqTable.SelectedItem;
                    if ((e.FreqKHz >= 100000 && e.FreqKHz < 500000) || (e.FreqKHz >= 500000 && e.FreqKHz < 2500000) || (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000))
                    {
                        FreqTable.Items.RemoveAt(ind);
                        if (e.FreqKHz >= 100000 && e.FreqKHz < 500000)
                        {
                            L = 1;
                            Descrip = "100-500";
                        }
                        else if (e.FreqKHz >= 500000 && e.FreqKHz < 2500000)
                        {
                            L = 2;
                            Descrip = "500-2500";
                        }
                        else if (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000)
                        {
                            L = 3;
                            Descrip = "2500-6000";
                        }
                        if (L != 0 && CheckLit(L) == 0)
                        {
                            if (e.Hindrance == TableFreq.hid.FirstParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power });
                            }
                            else if (e.Hindrance == TableFreq.hid.SecondParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power });
                            }
                            else if (e.Hindrance == TableFreq.hid.ThirdParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
                            }
                            else if (e.Hindrance == TableFreq.hid.FourthParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ2 {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
                            }
                            FreqTable.SelectedIndex = ind;
                            CreateConfig();
                            windowChange.Close();
                        }
                        else
                        {
                            FreqTable.Items.Insert(ind, item);
                            FreqTable.SelectedIndex = ind;
                            MessageBox.Show("Значение для литеры существует!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Проверьте вводимые данные!");
                    }
                }
            }
            catch { }
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                windowAdd = new AddWindowS();
                windowAdd.LocationChanged += WindowAdd_LocationChanged;
                windowAdd.OnAdd += Window_onAdd;
                MyWindowInitLocation(windowAdd);
                windowAdd.ShowDialog();
            }
            catch
            {
                windowAdd.Close();
            }
        }


        private int CheckLit(int e)
        {
            int count = 0;
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                if (e == item.Lit)
                    count++;
            }
            return count;
        }

        private void Window_onAdd(object sender, TableFreq e)
        {
            int L = 0;
            string Descrip = string.Empty;
            if ((e.FreqKHz >= 100000 && e.FreqKHz < 500000) || (e.FreqKHz >= 500000 && e.FreqKHz < 2500000) || (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000))
            {

                if (e.FreqKHz >= 100000 && e.FreqKHz < 500000)
                {
                    L = 1;
                    Descrip = "100-500";
                }
                else if (e.FreqKHz >= 500000 && e.FreqKHz < 2500000)
                {
                    L = 2;
                    Descrip = "500-2500";
                }
                else if (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000)
                {
                    L = 3;
                    Descrip = "2500-6000";
                }
                if (L != 0 && CheckLit(L) == 0)
                {
                    if (e.Hindrance == TableFreq.hid.FirstParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power });
                    }
                    else if (e.Hindrance == TableFreq.hid.SecondParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power });
                    }
                    else if (e.Hindrance == TableFreq.hid.ThirdParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
                    }
                    else if (e.Hindrance == TableFreq.hid.FourthParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ2 {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
                    }
                    FreqTable.SelectedIndex = FreqTable.Items.Count - 1;
                    myId++;
                    CreateConfig();
                    windowAdd.Close();
                }
                else
                {
                    MessageBox.Show("Значение для литеры существует!");
                }
            }
            else
            {
                MessageBox.Show("Проверьте вводимые данные!");
            }
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    item.Id = i + 1;
                }
                myId--;
                CreateConfig();
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FreqTable.Items.Clear();
            myId = 1;
            CreateConfig();
        }

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FreqTable.SelectedIndex > -1)
                {
                    windowChange = new ChangeWindowS();
                    windowChange.onChange += WindowChange_onChange;
                    windowChange.LocationChanged += WindowChange_LocationChanged;
                    var a = (TableFreq)FreqTable.SelectedItem;
                    Change?.Invoke(this, (TableFreq)FreqTable.SelectedItem);
                    MyWindowInitLocation(windowChange);
                    windowChange.ShowDialog();
                }
            }
            catch
            {
                windowChange.Close();
            }
        }
        #endregion

        #region STable
        private void ReadConfig()
        {
            try
            {
                using (FileStream fs = new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}STable.xml", FileMode.OpenOrCreate))
                {
                    TableFreq[] item = (TableFreq[])formatter.Deserialize(fs);

                    foreach (TableFreq p in item)
                    {
                        FreqTable.Items.Add(p);
                        myId++;
                    }
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                using (FileStream fs = new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}STable.xml", FileMode.Create))
                {
                    var a = new TableFreq[FreqTable.Items.Count];
                    var k = FreqTable.SelectedIndex;
                    for (int i = 0; i < a.Length; i++)
                    {
                        FreqTable.SelectedIndex = i;
                        a[i] = FreqTable.SelectedItem as TableFreq;
                    }
                    FreqTable.SelectedIndex = k;
                    formatter.Serialize(fs, a);
                }
            }
            catch { }
        }
        #endregion

        #region Location
        private void WindowAdd_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowAdd.Left);
                    sw.WriteLine(windowAdd.Top);
                }
            }
            catch { }
        }

        private void WindowChange_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowChange.Left);
                    sw.WriteLine(windowChange.Top);
                }
            }
            catch { }
        }

        private void MyWindowInitLocation(Window e)
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml"))
                {
                    e.Left = Convert.ToDouble(sr.ReadLine());
                    e.Top = Convert.ToDouble(sr.ReadLine());
                }
            }
            catch { }
        }
        #endregion

    }
}
