﻿using ClosedXML.Excel;
using Ethernet_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для CalibrationControl.xaml
    /// </summary>
    public partial class CalibrationControl : UserControl
    {
        public CalibrationControl()
        {
            InitializeComponent();
            TypeComboBox.Items.Add("Ethernet");
            TypeComboBox.Items.Add("USB");
            TypeComboBox.SelectedIndex = 0;
            ethernet.OnFreq += Ethernet_OnFreq;
            ethernet.OnMark += Ethernet_OnMark;
            ethernet.OnMarkXY += Ethernet_OnMarkXY;
            ethernet.OnText += Ethernet_OnText;
            ethernet.OnSaveMark += Ethernet_OnSaveMark;
            ethernet.OnRepeatCommand += Ethernet_OnRepeatCommand;
            ethernet.OnEndScan += Ethernet_OnEndScan;
            MainWindow.StartScan += MainWindow_StartScan;
            MainWindow.OnConfigEthernet += Config;
        }

        private void Ethernet_OnSaveMark(object sender, List<MarkModel> e)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    string path = $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Отчет\";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    using (var wbook = new XLWorkbook())
                    {
                        var ws = wbook.Worksheets.Add($"{DateTime.Now:HHmmss}");

                        for (int i = 0; i < e.Count; i++)
                        {
                            ws.Cell($"A{i + 1}").Value = Convert.ToDouble(e[i].XMARKData);
                            ws.Cell($"B{i + 1}").Value = Convert.ToDouble(e[i].YMARKData);
                        }

                        wbook.SaveAs(path + $@"{DateTime.Now:HH_mm_ss}.xlsx");
                    }
                });
            }
            catch
            {

            }
        }

        private void Ethernet_OnEndScan(object sender, bool e)
        {
            Dispatcher.Invoke(() => {
                MainWindow.AnswerForEthernet = false;
                MainWindow.bCount = false;
                OnStopScan?.Invoke(this, (bool)AmplZButton.IsChecked);
            });
        }

        private void Ethernet_OnText(object sender, string e)
        {
            Dispatcher.Invoke(() => {
                OnAddTextToDisplay?.Invoke(this, e);
            });
        }

        private void Ethernet_OnRepeatCommand(object sender, int e)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    StartFreqBox.Text = (e / 1000).ToString();
                    OnStartScan?.Invoke(this, new ForCalibrationControl(Convert.ToInt16(StartFreqBox.Text) * 1000, (bool)UUButton.IsChecked, (bool)AmplSButton.IsChecked, (bool)AmplZButton.IsChecked, (bool)PowerButton.IsChecked));
                });
            }
            catch { }
        }

        private void MainWindow_StartScan(object sender, bool e)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    var type = (Ethernet.ConnectionType)TypeComboBox.SelectedIndex;
                    var bType = type == Ethernet.ConnectionType.Ethernet ? true : false;
                    ethernet.SendFreq(Convert.ToInt32(StartFreqBox.Text), Convert.ToInt32(SetSpanBox.Text), Convert.ToInt32(StopFreqBox.Text),
                                                                                (bool)AvrgToggleButton.IsChecked, Convert.ToInt16(AvrgTextBox.Text), bType);
                });
            }
            catch
            {

            }
        }

        private void Ethernet_OnMarkXY(object sender, string e)
        {
            OnAddTextToDisplay?.Invoke(this, e);
        }

        private void Ethernet_OnMark(object sender, string e)
        {
            OnAddTextToDisplay?.Invoke(this, e);
        }

        private void Ethernet_OnFreq(object sender, string e)
        {
            OnAddTextToDisplay?.Invoke(this, e);
        }

        public event EventHandler<string> OnAddTextToDisplay;
        public event EventHandler<ForCalibrationControl> OnSetParam;
        public event EventHandler<ForCalibrationControl> OnStartScan;
        public event EventHandler<bool> OnStopScan;
        private Ethernet ethernet = new Ethernet();
        private string typeConnection = string.Empty;

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReadConfig((Ethernet.ConnectionType)TypeComboBox.SelectedIndex);
                Dispatcher.Invoke(() =>
                {
                    OnAddTextToDisplay?.Invoke(this, ethernet.Connect((Ethernet.ConnectionType)TypeComboBox.SelectedIndex, typeConnection));
                });
            }
            catch
            {

            }
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ethernet.Close())
                    OnAddTextToDisplay?.Invoke(this, "Disconnect!");
                else
                    OnAddTextToDisplay?.Invoke(this, "Error Disconnect!");
            }
            catch
            {

            }
        }

        private void ReadConfig(Ethernet.ConnectionType type)
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}KeysightConfig.txt"))
                {
                    if (type == Ethernet.ConnectionType.Ethernet)
                        typeConnection = sr.ReadLine();
                    else
                    {
                        typeConnection = sr.ReadLine();
                        typeConnection = sr.ReadLine();
                    }
                }
            }
            catch
            {
                CreateConfig(type);
            }
        }

        private void CreateConfig(Ethernet.ConnectionType type)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}KeysightConfig.txt");
                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("TCPIP0::10.101.25.135::hislip0::INSTR");
                    sw.WriteLine("USB0::0x0957::0xFFEF::CN0604B269::0::INSTR");
                }
                ReadConfig(type);
            }
            catch { }
        }

        private void RefAmpButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var answer = ethernet.RefAmp(RefAmpBox.Text);
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void RefOffsetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = (Ethernet.ConnectionType)TypeComboBox.SelectedIndex;
                var bType = type == Ethernet.ConnectionType.Ethernet ? true : false;
                var answer = ethernet.RefOffset(RefOffsetBox.Text, bType);
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void SetSpanButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var answer = ethernet.SetSpan(SetSpanBox.Text);
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void SysResetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = (Ethernet.ConnectionType)TypeComboBox.SelectedIndex;
                var bType = type == Ethernet.ConnectionType.Ethernet ? true : false;
                var answer = string.Empty;
                if (ethernet.SysReset(bType))
                    answer = "Sys Reset Done!";
                else
                    answer = "Sys Reset Error!";
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void SetPeakButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = (Ethernet.ConnectionType)TypeComboBox.SelectedIndex;
                var bType = type == Ethernet.ConnectionType.Ethernet ? true : false;
                var answer = string.Empty;
                if (ethernet.SetPeak(bType))
                    answer = "Set Peak Done!";
                else
                    answer = "Set Peak Error!";
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void SetParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSetParam?.Invoke(this, new ForCalibrationControl(Convert.ToInt16(StartFreqBox.Text) * 1000, (bool)UUButton.IsChecked, (bool)AmplSButton.IsChecked, (bool)AmplZButton.IsChecked, (bool)PowerButton.IsChecked));
            }
            catch
            {

            }
        }

        private void GetDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var type = (Ethernet.ConnectionType)TypeComboBox.SelectedIndex;
                var bType = type == Ethernet.ConnectionType.Ethernet ? true : false;
                var answer = string.Empty;
                if (ethernet.SetPeak(bType))
                    answer = "Get Data Done!";
                else
                    answer = "Get Data Error!";
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void SetKSButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var answer = string.Empty;
                if (ethernet.SetKS(StartFreqBox.Text + "000"))
                    answer = "Set KS Done!";
                else
                    answer = "Set KS Error!";
                OnAddTextToDisplay?.Invoke(this, answer);
            }
            catch
            {

            }
        }

        private void StartScanButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ethernet.startScan = true;
                MainWindow.AnswerForEthernet = true;
                OnStartScan?.Invoke(this, new ForCalibrationControl(Convert.ToInt16(StartFreqBox.Text) * 1000, (bool)UUButton.IsChecked, (bool)AmplSButton.IsChecked, (bool)AmplZButton.IsChecked, (bool)PowerButton.IsChecked));
            }
            catch { }
        }

        private void StopScanButton_Click(object sender, RoutedEventArgs e)
        {
            ethernet.startScan = false;
            MainWindow.AnswerForEthernet = false;
            MainWindow.bCount = false;
            ethernet.SaveMark();
            OnStopScan?.Invoke(this, (bool)AmplZButton.IsChecked);
        }

        private void Config(object sender, bool readWrite)
        {
            try
            {
                if (readWrite)
                {
                    using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}ConfigKeysight.xml"))
                    {
                        _ = sr.ReadLine() == "USB" ? TypeComboBox.SelectedIndex = 1 : TypeComboBox.SelectedIndex = 0;
                        RefAmpBox.Text = sr.ReadLine();
                        RefOffsetBox.Text = sr.ReadLine();
                        SetSpanBox.Text = sr.ReadLine();
                        AvrgTextBox.Text = sr.ReadLine();
                        FreqBox.Text = sr.ReadLine();
                        StartFreqBox.Text = sr.ReadLine();
                        StopFreqBox.Text = sr.ReadLine();
                    }
                }
                else
                {
                    string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}ConfigKeysight.xml");
                    using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                    {
                        sw.WriteLine(TypeComboBox.SelectedIndex == 1 ? "USB" : "Ethernet");
                        sw.WriteLine(RefAmpBox.Text);
                        sw.WriteLine(RefOffsetBox.Text);
                        sw.WriteLine(SetSpanBox.Text);
                        sw.WriteLine(AvrgTextBox.Text);
                        sw.WriteLine(FreqBox.Text);
                        sw.WriteLine(StartFreqBox.Text);
                        sw.WriteLine(StopFreqBox.Text);
                    }
                }
            }
            catch
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}ConfigKeysight.xml");
                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("USB");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                }
                Config(this, true);
            }
        }

        private void OffRadButton_Click(object sender, RoutedEventArgs e)
        {
            OnStopScan?.Invoke(this, (bool)AmplZButton.IsChecked);
        }

        private void AmplSButton_Click(object sender, RoutedEventArgs e)
        {
            AmplZButton.IsEnabled = AmplSButton.IsChecked == true ? false : true;
            UUButton.IsEnabled = AmplSButton.IsChecked == true ? false : true;
        }

        private void AmplZButton_Click(object sender, RoutedEventArgs e)
        {
            AmplSButton.IsEnabled = AmplZButton.IsChecked == true ? false : true;
            UUButton.IsEnabled = AmplZButton.IsChecked == true ? false : true;
        }

        private void UUButton_Click(object sender, RoutedEventArgs e)
        {
            AmplZButton.IsEnabled = UUButton.IsChecked == true ? false : true;
            AmplSButton.IsEnabled = UUButton.IsChecked == true ? false : true;
        }
    }
}
