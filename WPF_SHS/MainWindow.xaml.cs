﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Windows;
using System.Windows.Media;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MyWindowInitLocation();
            GrozaSButton.Background = Brushes.White;
            GrozaZ1Button.Background = Brushes.DarkGray;
            CalibrationButton.Background = Brushes.DarkGray;
            GrozaZ1.Visibility = Visibility.Collapsed;
            Calibration.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
            IntitComPort();
            OnConfigEthernet?.Invoke(this, true);
            ReadConfig();

            sHS.OnWriteByte += SHS_OnWriteByte;
            sHS.OnReadByte += SHS_OnReadByte;
            sHS.OnDisconnect += SHS_OnDisconnect;

            GrozaS.OnStatus += GrozaS_OnStatus;
            GrozaS.OnStatusAntenna += GrozaS_OnStatusAntenna;
            GrozaS.OnSwitch += GrozaS_OnSwitch;
            GrozaS.OnSetNaVi += GrozaS_OnSetNaVi;
            GrozaS.OnSetGNSS += GrozaS_OnSetGNSS;
            GrozaS.OnParam += GrozaS_OnParam;
            GrozaS.OnSpoof += GrozaS_OnSpoof;
            GrozaS.OnOffRad += GrozaS_OnOffRad;
            GrozaS.OnReset += GrozaS_OnReset;
            GrozaS.OnRelaySwitching += GrozaS_OnRelaySwitching;

            GrozaF.OnSetParam += GrozaF_OnSetParam;
            GrozaF.OnGetParam += GrozaF_OnGetParam;
            GrozaF.OnSaveParam += GrozaF_OnSaveParam;
            GrozaF.OnStatus += GrozaF_OnStatus;
            GrozaF.OnRadOFF += GrozaF_OnRadOFF;
            GrozaF.OnInterParam += GrozaF_OnInterParam;
            GrozaF.OnGNSS += GrozaF_OnGNSS;
            GrozaF.OnSwitchPosition += GrozaF_OnSwitchPosition;
            GrozaF.OnNaVi += GrozaF_OnNaVi;
            GrozaF.OnVersion += GrozaF_OnVersion;

            GrozaZ1.OnFullStatus += GrozaZ1_OnFullStatus;
            GrozaZ1.OnOffRadAndSetPres += GrozaZ1_OnOffRadAndSetPres;
            GrozaZ1.OnSetPres += GrozaZ1_OnSetPres;
            GrozaZ1.OnSetParam += GrozaZ1_OnSetParam;
            GrozaZ1.OnSwitch += GrozaS_OnSwitch;

            Calibration.OnAddTextToDisplay += Calibration_OnAddTextToDisplay;
            Calibration.OnSetParam += Calibration_OnFreq;
            Calibration.OnStartScan += Calibration_OnStartScan;
            Calibration.OnStopScan += Calibration_OnStopScan;

            sHS.OnConfirmStatus += SHS_OnConfirmStatus;
            sHS.OnConfirmStatusAntenna += SHS_OnConfirmStatusAntenna;
            sHS.OnConfirmSet += SHS_OnConfirmSet;
            sHS.OnConfirmFullStatus += SHS_OnConfirmFullStatus;
            sHS.OnConfirmSetParam += SHS_OnConfirmSetParam;
            sHS.OnConfirmSaveParam += SHS_OnConfirmSaveParam;
            sHS.OnConfirmGetParam += SHS_OnConfirmGetParam;
            sHS.OnStatus += SHS_OnStatus;
            sHS.OnRadiatOff += SHS_OnRadiatOff;
            sHS.OnSetIRI += SHS_OnSetIRI;
            sHS.OnTestGNSS += SHS_OnTestGNSS;
            sHS.OnSwitchPosition += SHS_OnSwitchPosition;
            sHS.OnParamNaVi += SHS_OnParamNaVi;
            sHS.OnVersion += SHS_OnVersion;
            sHS.OnConfirmSwitch += SHS_OnConfirmSwitch;

            Display.DisplayTime = false;
        }

        private void GrozaS_OnSetGNSS(object sender, SetGNSSEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить подавляемые системы навигации и включить излучение 2", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetGNSS(new TGpsGlonass() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 }, new TBeidouGalileo() { beidouL1 = e.Beidou_L1, beidouL2 = e.Beidou_L2, galileoL1 = e.Galileo_L1, galileoL2 = e.Galileo_L2 });
                }
                else
                {
                    sHS.SendSetGNSS(new TGpsGlonass() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 }, new TBeidouGalileo() { beidouL1 = e.Beidou_L1, beidouL2 = e.Beidou_L2, galileoL1 = e.Galileo_L1, galileoL2 = e.Galileo_L2 }, e.PowerByte);
                }
            }
            catch { }
        }

        private void Calibration_OnStartScan(object sender, ForCalibrationControl e)
        {
            var paramFWS = new TParamFWS[] { new TParamFWS() { Freq = (uint)e.Freq, Deviation = 0, Duration = 0, Manipulation = 0, Modulation = 0 } };

            if (!bCount)
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Cтарт сканирования", Brushes.Red);
                bCount = true;
            }

            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);

            if (e.UU)
            {
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendSetParamsIRI(paramFWS, e.UU);
            }
            else if(e.AmplS)
            {
                if (e.Power == false)
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                }
                else
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS, new byte[100]);
                }
            }
            else if(e.AmplZ)
            {
                sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), new TGpsGlonass(), new TBeidouGalileo(), paramFWS, new byte[100], 100);
            }
        }

        private void Calibration_OnStopScan(object sender, bool e)
        {
            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Выключить излучение", Brushes.Red);
            if (!e)
                sHS.SendRadiatOff(0);
            else
                sHS.SendPreselectorOn(0);
        }

        public static event EventHandler<bool> StartScan;
        public static bool bCount = false;

        private void Calibration_OnFreq(object sender, ForCalibrationControl e)
        {
            var paramFWS = new TParamFWS[] { new TParamFWS() { Freq = (uint)e.Freq, Deviation = 0, Duration = 0, Manipulation = 0, Modulation = 0 } };

            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);

            if (e.UU)
            {
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendSetParamsIRI(paramFWS, e.UU);
            }
            else if (e.AmplS)
            {
                if (e.Power == false)
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                }
                else
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS, new byte[100]);
                }
            }
            else if (e.AmplZ)
            {
                sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), new TGpsGlonass(), new TBeidouGalileo(), paramFWS, new byte[100], 100);
            }
        }

        public static bool AnswerForEthernet = false;

        private void Calibration_OnAddTextToDisplay(object sender, string e)
        {
            Dispatcher.Invoke(() => { Display.AddTextToLog($"{e}", Brushes.Green); });
        }

        private void SHS_OnVersion(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Запросить версию ПО", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Версия: {e}", Brushes.Green); });
            }
        }

        private void SHS_OnConfirmSwitch(object sender, ConfirmSetSwitch e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Номер сектора: {e.SectorNumber[0]}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Номер сектора: {e.SectorNumber[1]}", Brushes.Green); });
            }
        }

        private void GrozaF_OnVersion(object sender, EventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Запросить версию ПО", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendGetVersion(GrozaF.UU);
            }
            catch { }
        }

        private void SHS_OnParamNaVi(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Установить параметры помехи для навигации и включить излучение", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        private void GrozaF_OnNaVi(object sender, byte e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры помехи для навигации и включить излучение", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendSetParamNaViRadOn(GrozaF.UU, e);
            }
            catch { }
        }

        private void SHS_OnSwitchPosition(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Установить положение коммутаторов", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        private void GrozaF_OnSwitchPosition(object sender, byte[] e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить положение коммутаторов", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendPositionSwitches(GrozaF.UU, e[0], e[1]);
            }
            catch { }
        }

        private void SHS_OnTestGNSS(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Установить тестовый сигнал для GNSS без модуляции", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        private void GrozaF_OnGNSS(object sender, EventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить тестовый сигнал для GNSS без модуляции", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendSetTestGNSS(GrozaF.UU);
            }
            catch { }
        }

        private void SHS_OnSetIRI(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Установить параметры помехи ИРИ и включить излучение", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
            if (AnswerForEthernet)
                Dispatcher.Invoke(() => { StartScan?.Invoke(this, true); });
        }

        private void GrozaF_OnInterParam(object sender, byte time)
        {
            TParamFWS[] paramFWS;
            List<TableFreq> e = new List<TableFreq>();
            if (GrozaS.Visibility == Visibility.Visible)
            {
                e = GrozaS.GetTableS();
            }
            else if (GrozaZ1.Visibility == Visibility.Visible)
            {
                e = GrozaZ1.GetTableZ1();
            }

            paramFWS = new TParamFWS[e.Count];

            for (int i = 0; i < e.Count; i++)
            {
                if (e[i].Hindrance == TableFreq.hid.FirstParam)
                {
                    paramFWS[i] = new TParamFWS() { Freq = (uint)e[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.SecondParam)
                {
                    paramFWS[i] = new TParamFWS() { Freq = (uint)e[i].FreqKHz, Deviation = 0, Manipulation = (byte)e[i].Man, Modulation = 1, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.ThirdParam)
                {
                    paramFWS[i] = new TParamFWS() { Freq = (uint)e[i].FreqKHz, Deviation = e[i].Dev, Manipulation = (byte)e[i].Man, Modulation = 4, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.FourthParam)
                {
                    paramFWS[i] = new TParamFWS() { Freq = (uint)e[i].FreqKHz, Deviation = e[i].Dev, Manipulation = (byte)e[i].Man, Modulation = 5, Duration = time };
                }
            }
            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры помехи ИРИ и включить излучение", Brushes.Red);
            sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
            sHS.SendSetParamsIRI(paramFWS, GrozaF.UU);
        }

        private void SHS_OnRadiatOff(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Выключить излучение", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        private void GrozaF_OnRadOFF(object sender, byte e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendRadiatOFF(e, GrozaF.UU);
            }
            catch { }
        }

        private void SHS_OnStatus(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Статус формирователя", Brushes.Green); });
                Dispatcher.Invoke(() =>
                {
                    string b = Convert.ToString(e, 2);
                    b = b.PadLeft(8, '0');
                    Display.AddTextToLog($"ADF1 {b[7]}", Brushes.Green);
                    Display.AddTextToLog($"ADF2 {b[6]}", Brushes.Green);
                    Display.AddTextToLog($"ADF3 {b[5]}", Brushes.Green);
                    Display.AddTextToLog($"ADF4 {b[4]}", Brushes.Green);
                    if (GrozaF.OneOf)
                    {
                        Display.AddTextToLog($"Нет ВЧ1 L1 L2 {b[2]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ2 {b[1]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ3 100-500 {b[0]}", Brushes.Green);
                    }
                    else
                    {
                        Display.AddTextToLog($"Нет ВЧ1 {b[2]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ2 {b[1]}", Brushes.Green);
                    }
                });
            }
        }

        private void GrozaF_OnStatus(object sender, EventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус формирователя", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendDeviceStatus(GrozaF.UU);
            }
            catch { }
        }

        private void SHS_OnConfirmSaveParam(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Сохранить в параметры усилителя в канале", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        public static bool activiti = true; //true - S false - Z1
        public static event EventHandler<FTable[]> GetParams;

        private void SHS_OnConfirmGetParam(object sender, ConfirmGetParamEventArgs[] e)
        {
            FTable[] fTables = new FTable[e.Length];
            Dispatcher.Invoke(() => {
                for (int i = 0; i < e.Length; i++)
                {
                    fTables[i] = new FTable() { Register = e[i].Id, FStart = e[i].Fl, FStop = e[i].Fh, KStart = e[i].Kl, KStop = e[i].Kh, Status = e[i].Status };
                }
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Запросить параметр усилителя в канале", Brushes.Green); });
                }
                GetParams?.Invoke(this, fTables);
            });
        }

        private void SHS_OnConfirmSetParam(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Установить параметр усилителя в канале", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e}", Brushes.Green); });
            }
        }

        private void GrozaF_OnSaveParam(object sender, EventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Сохранить в параметры усилителя в канале", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendSaveParam(GrozaF.UU);
            }
            catch
            {

            }
        }

        private void GrozaF_OnGetParam(object sender, byte[] e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Запросить параметр усилителя в канале", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                sHS.SendGetParam(e, GrozaF.UU);
            }
            catch
            {

            }
        }

        private void GrozaF_OnSetParam(object sender, FTable[] e)
        {
            try
            {
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                var paramFPS = new FPSParam[e.Length];
                for (int i = 0; i < e.Length; i++)
                {
                    paramFPS[i] = new FPSParam() { Id = e[i].Register, FreqH = (uint)e[i].FStop, FreqL = (uint)e[i].FStart, KH = e[i].KStop, KL = e[i].KStart };
                }
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметр усилителя в канале", Brushes.Red);
                sHS.SendParam(paramFPS, GrozaF.UU);
            }
            catch
            {

            }
        }

        #region Config
        private string COM;
        private string Baudrate;

        private void ReadConfig()
        {
            try
            {
                int count = 0;
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}Config.xml"))
                {
                    COM = sr.ReadLine();
                    Baudrate = sr.ReadLine();
                }
                foreach (var i in ComBox.Items)
                {
                    if (i.Equals(COM))
                        ComBox.SelectedIndex = count;
                    count++;
                }
                count = 0;
                foreach (var i in RateBox.Items)
                {
                    if (i.Equals(Baudrate))
                        RateBox.SelectedIndex = count;
                    count++;
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}Config.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(COM);
                    sw.WriteLine(Baudrate);
                }
            }
            catch { }
        }
        #endregion

        #region Read, Write
        private void SHS_OnReadByte(object sender, ByteEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(() => { ControlConnection.ShowRead(); });
                string a = "";
                foreach (var i in e.Byte)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Dispatcher.Invoke(() => { Display.AddTextToLog(a, Brushes.Green); });
            }
            catch
            {

            }
        }

        private void SHS_OnWriteByte(object sender, ByteEventArgs e)
        {
            try
            {
                ControlConnection.ShowWrite();
                string a = "";
                foreach (var i in e.Byte)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Display.AddTextToLog(a, Brushes.Red);
            }
            catch { }
        }
        #endregion

        #region Private GrozaS
        private bool CommandSetS = false;
        private bool[] S = new bool[] { false, false, false };
        private bool CommandResetS = false;
        #endregion

        #region Private GrozaZ1
        private bool CommandSet = false;
        private bool[] Z = new bool[] { false, false, false, false, false };
        private bool CommandReset = false;
        #endregion

        #region CommandForCrozaZ1
        private void GrozaZ1_OnSetParam(object sender, SetParamFWSGNSSEventArgs e)
        {
            try
            {
                var paramFWS = new TParamFWS[e.Param.Tables.Count];
                for (int i = 0; i < e.Param.Tables.Count; i++)
                {
                    if (e.Param.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0 };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 1 };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = e.Param.Tables[i].Dev, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 4 };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.FourthParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = e.Param.Tables[i].Dev, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 5 };
                    }
                }
                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 350000 && i.Freq <= 500000)
                    {
                        Z[0] = true;
                    }
                    else if (i.Freq >= 800000 && i.Freq <= 1500000)
                    {
                        Z[1] = true;
                    }
                    else if (i.Freq >= 2000000 && i.Freq <= 2700000)
                    {
                        Z[2] = true;
                    }
                    else if (i.Freq >= 5600000 && i.Freq <= 5900000)
                    {
                        Z[3] = true;
                    }
                }
                Z[4] = true;
                CommandSet = true;
                var gnss = new TGpsGlonass() { gpsL1 = e.NaVi.GPS_L1, gpsL2 = e.NaVi.GPS_L2, glnssL1 = e.NaVi.Glonass_L1, glnssL2 = e.NaVi.Glonass_L2 };
                var beidouGalileo = new TBeidouGalileo() { beidouL1 = e.NaVi.Beidou_L1, beidouL2 = e.NaVi.Beidou_L2, galileoL1 = e.NaVi.Galileo_L1, galileoL2 = e.NaVi.Galileo_L2 };
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и системы навигации. Включить излучение заданной длительности", Brushes.Red);
                sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), gnss, beidouGalileo, paramFWS, e.Power, e.NaVi.PowerByte);
            }
            catch { }
        }

        private void GrozaZ1_OnSetPres(object sender, SetPresEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Настройка преселектора", Brushes.Red);
                sHS.SendParamPreselector(e.Pres);
            }
            catch { }
        }

        private void GrozaZ1_OnOffRadAndSetPres(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение и Включить преселектор", Brushes.Red);
                sHS.SendPreselectorOn(e.Letter);
                CommandReset = true;
            }
            catch { }
        }
        #endregion

        #region Full Status
        public static event EventHandler<ConfirmFullStatusEventArgs> OnAddFullStatusToSecondTable;

        private void SHS_OnConfirmFullStatus(object sender, ConfirmFullStatusEventArgs e)
        {
            try
            {
                if (AnswerForEthernet && e.Amp == AmpCodes.FULL_STATUS)
                {
                    Dispatcher.Invoke(() => { StartScan?.Invoke(this, true); });
                    return;
                }

                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.Amp}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                }
                string a = e.Relay ? "ON" : "OFF";
                Dispatcher.Invoke(() => { RelayLabel.Content = a; });

                if (CommandSet/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaZ1.ZLit[i] = true;
                        }
                    }
                    CommandSet = false;
                }

                if (CommandReset/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 &&*/Z[i])
                        {
                            GrozaZ1.ZLit[i] = false;
                            Z[i] = false;
                        }
                    }
                    CommandReset = false;
                }
                OnAddFullStatusToSecondTable?.Invoke(this, e);
            }
            catch
            { }
        }

        private void GrozaZ1_OnFullStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Полный статус", Brushes.Red);
                sHS.SendFullStatus(e.Letter);
            }
            catch { }
        }
        #endregion

        #region CommandForGrozaS

        #region RelaySwitch
        private void GrozaS_OnRelaySwitching(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Переключение реле", Brushes.Red);
                sHS.SendRelaySwitching(e.Letter);
            }
            catch { }
        }
        #endregion

        private void GrozaS_OnParam(object sender, ParamEventArgs e)
        {
            try
            {
                var paramFWS = new TParamFWS[e.Tables.Count];
                for (int i = 0; i < e.Tables.Count; i++)
                {
                    if (e.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Tables[i].Man, Modulation = 1 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 4 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.FourthParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 5 };
                    }
                }

                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 100000 && i.Freq < 500000)
                    {
                        S[0] = true;
                    }
                    else if (i.Freq >= 500000 && i.Freq < 2500000)
                    {
                        S[1] = true;
                    }
                    else if (i.Freq >= 2500000 && i.Freq < 6000000)
                    {
                        S[2] = true;
                    }
                }
                CommandSetS = true;

                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                }
                else
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS, e.PowerByte);
                }
            }
            catch { }
        }

        #region Reset
        private void GrozaS_OnReset(object sender, LetterEventArgs e)
        {
            try
            {
                CommandResetS = true;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Сброс", Brushes.Red);
                sHS.SendReset(e.Letter);
            }
            catch { }
        }
        #endregion

        #region Off radiat
        private void GrozaS_OnOffRad(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                sHS.SendRadiatOff(e.Letter);
            }
            catch { }
        }
        #endregion

        #region Spoof
        private void GrozaS_OnSpoof(object sender, SetSpoofEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Включить спуфинг", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetSPOOF();
                }
                else
                {
                    sHS.SendSetSPOOF(e.PowerByte);
                }
            }
            catch { }
        }
        #endregion

        #region SetGnss
        private void SHS_OnConfirmSet(object sender, ConfirmSetEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                }
                if (AnswerForEthernet && e.AmpCode == AmpCodes.PARAM_FWS_APP)
                    Dispatcher.Invoke(() => { StartScan?.Invoke(this, true); });
            }
            catch { }
        }

        private void GrozaS_OnSetNaVi(object sender, SetNaViEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установка подавляемых систем навигации и включение излучения", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetGNSS(new TGpsGlonass() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 });
                }
                else
                {
                    sHS.SendSetGNSS(new TGpsGlonass() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 }, e.PowerByte);
                }
            }
            catch { }
        }


        #endregion

        #region Status
        public static event EventHandler<ConfirmStatusEventArgs> OnAddStatusToSecondTable;
        private void SHS_OnConfirmStatus(object sender, ConfirmStatusEventArgs e)
        {
            try
            {
                if (CommandSet/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaS.SLit[i] = true;
                        }
                    }
                    CommandSetS = false;
                }

                if (CommandReset/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaS.SLit[i] = false;
                            S[i] = false;
                        }
                    }
                    CommandResetS = false;
                }
                OnAddStatusToSecondTable?.Invoke(this, e);
            }
            catch { }
        }

        private void SHS_OnConfirmStatusAntenna(object sender, ConfirmStatusEventArgs e)
        {
            try
            {
                if (CommandSet)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = true;
                        }
                    }
                    CommandSetS = false;
                }

                if (CommandReset)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = false;
                            S[i] = false;
                        }
                    }
                    CommandResetS = false;
                }

                string Antenna = string.Empty;

                Dispatcher.Invoke(() => {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        Antenna = e.ParamAmp[i].Antenna == 0 ? "Направленная" : "Ненаправленная";
                        Display.AddTextToLog($"Антенна литера {i + 1}: {Antenna}", Brushes.Green);
                    }

                });
                OnAddStatusToSecondTable?.Invoke(this, e);
            }
            catch { }
        }

        private void GrozaS_OnStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                sHS.SendStatus(e.Letter);
            }
            catch { }
        }

        private void GrozaS_OnStatusAntenna(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                sHS.SendStatusAntennaState(e.Letter);
            }
            catch { }
        }

        private void GrozaS_OnSwitch(object sender, SwitchEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog( e.ReceiveTransmit == true ? "Настройка приёмных коммутаторов" : "Настройка передающих коммутаторов", Brushes.Red);

                if (e.ReceiveTransmit)
                {
                    sHS.SendSetReceivingSwitches(e.SectorNumber);
                }
                else
                {
                    sHS.SendSetTransmissionSwitches(e.SectorNumber);
                }
            }
            catch { }
        }
        #endregion

        #endregion

        private void SHS_OnDisconnect(object sender, bool e)
        {
            try
            {
                if (e)
                {
                    connectFlag = false;
                    Dispatcher.Invoke(() => { ControlConnection.ShowDisconnect(); });
                }
            }
            catch { }
        }

        public ComSHS sHS = new ComSHS(0x04, 0x05);

        private void IntitComPort()
        {
            RelayLabel.Content = "Null";
            int[] k = new int[201];

            for (int i = 0; i < 201; i++)
            {
                k[i] = i;
            }

            //foreach(var a in k)
            //{
            //    TimeBox.Items.Add(a.ToString());
            //}
            //TimeBox.SelectedIndex = 0;

            TimeBox.Value = 0;
            TimeBox.Minimum = 0;
            TimeBox.Maximum = 200;

            foreach (string PortName in SerialPort.GetPortNames())
            {
                ComBox.Items.Add(PortName);
            }
            ComBox.SelectedIndex = 0;

            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";

            foreach (string baudRate in listbaudrate)
            {
                RateBox.Items.Add(baudRate);
            }
            RateBox.SelectedIndex = 6;
        }

        private void GrozaSButton_Click(object sender, RoutedEventArgs e)
        {
            GrozaSButton.Background = Brushes.White;
            GrozaZ1Button.Background = Brushes.DarkGray;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
            GrozaF.Visibility = Visibility.Visible;
            Calibration.Visibility = Visibility.Collapsed;
            CalibrationButton.Background = Brushes.DarkGray;
            TimeBox.Visibility = Visibility.Visible;
            TimeBoxLabel.Visibility = Visibility.Visible;
        }

        private void GrozaZ1Button_Click(object sender, RoutedEventArgs e)
        {
            GrozaZ1Button.Background = Brushes.White;
            GrozaSButton.Background = Brushes.DarkGray;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Visible;
            GrozaF.Visibility = Visibility.Visible;
            Calibration.Visibility = Visibility.Collapsed;
            CalibrationButton.Background = Brushes.DarkGray;
            TimeBox.Visibility = Visibility.Visible;
            TimeBoxLabel.Visibility = Visibility.Visible;
        }

        private bool connectFlag = true;

        private void ControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (connectFlag)
                {
                    connectFlag = sHS.OpenPort(ComBox.SelectedItem.ToString(), Convert.ToInt32(RateBox.SelectedItem), Parity.None, 8, StopBits.One);
                    if (connectFlag)
                    {
                        ControlConnection.ShowConnect();
                        ControlConnection.ShowRead();
                        ControlConnection.ShowWrite();
                        COM = ComBox.SelectedItem.ToString();
                        Baudrate = RateBox.SelectedItem.ToString();
                        CreateConfig();
                    }
                    connectFlag = false;
                }
                else
                {
                    connectFlag = sHS.ClosePort();
                    ControlConnection.ShowDisconnect();
                    connectFlag = true;
                }
            }
            catch { }
        }

        public static event EventHandler<bool> OnConfigEthernet;

        private void MyWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationMain.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(MyWindow.Left);
                    sw.WriteLine(MyWindow.Top);
                }
                OnConfigEthernet?.Invoke(this, false);
                System.Windows.Threading.Dispatcher.ExitAllFrames();
            }
            catch { }
        }

        private void MyWindowInitLocation()
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}LocationMain.txt"))
                {
                    MyWindow.Left = Convert.ToDouble(sr.ReadLine());
                    MyWindow.Top = Convert.ToDouble(sr.ReadLine());
                }
            }
            catch { }
        }

        private void CalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            GrozaZ1Button.Background = Brushes.DarkGray;
            GrozaSButton.Background = Brushes.DarkGray;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaF.Visibility = Visibility.Collapsed;
            Calibration.Visibility = Visibility.Visible;
            CalibrationButton.Background = Brushes.White;
            TimeBox.Visibility = Visibility.Collapsed;
            TimeBoxLabel.Visibility = Visibility.Collapsed;
        }
    }
}
