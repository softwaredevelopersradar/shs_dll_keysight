﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace WPF_SHS
{
    public class CheckGrozaZ1ToImgSourceConverter : IValueConverter
    {
        public static bool[] flag = new bool[5];
        public static int i = 0;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!flag[i])
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/gray.png", UriKind.RelativeOrAbsolute));
                }
            }
            else if (flag[i])
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute));
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class CheckGrozaZ1ValueToImgSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                CheckGrozaZ1ToImgSourceConverter.i++;
                return (bool)value ? new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
