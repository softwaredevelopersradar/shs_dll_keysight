﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace WPF_SHS
{
    public class GrozaFConverter : IValueConverter
    {
        public static bool checkBool = false;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!checkBool)
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/gray.png", UriKind.RelativeOrAbsolute));
                }
            }
            else if (checkBool)
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute));
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
