﻿namespace SHS_DLL
{
    #region Structures and Enum
    public struct TServicePartSHS
    {
        public byte AddrSend;
        public byte AddrReceive;
        public byte Code;
        public byte Counter;
        public byte Length;
    }

    public struct FPSServicePartSHS
    {
        public byte AddrSendReceiv;
        public byte Code;
        public byte Length;
    }

    public struct TParamAmp
    {
        public byte Snt;
        public byte Rad;
        public sbyte Temp;
        public byte Current;
        public byte Power;
        public byte Error;
        public byte Antenna;
    }

    public struct TParamFWS
    {
        public uint Freq;           // частота
        public byte Modulation;    // вид модуляции
        public int Deviation;     // вид девиации
        public byte Manipulation;  // вид манипуляции           
        public byte Duration;      // длительность  
    }

    public struct FPSParam
    {
        public byte Id;
        public uint FreqL;           // частота нижняя
        public uint FreqH;           // частота верхняя
        public byte KL;              // коэф нижний
        public byte KH;              // коэф верхний
    }

    public struct TGpsGlonass
    {
        public bool gpsL1;
        public bool gpsL2;
        public bool glnssL1;
        public bool glnssL2;
    }

    public struct TBeidouGalileo
    {
        public bool beidouL1;
        public bool beidouL2;
        public bool galileoL1;
        public bool galileoL2;
    }

    public enum FPSCodes
    {
        STATUS = 0x04,
        RADIAT_OFF = 0x24,
        PARAM_IRI_RADIAT_ON = 0x28,
        TEST_GNSS = 0x30,
        PARAM_NAVI_RADIAT_ON = 0x34,
        SET_PARAM_AMPL = 0x40,
        GET_PARAM_AMPL = 0x41,
        SAVE_PARAM_AMPL = 0x42,
        GET_PO_VERSION = 0x43,
        SWITCH_POSITION = 0x31
    }

    public enum AmpCodes
    {
        RELAY_SWITCHING = 22,
        RADIAT_OFF_APP = 13,
        STATUS_APP,
        PARAM_FWS_APP,
        RESET_APP,
        GNSS_APP,
        NAVI_RADIAT_ON,
        SPOOF_APP = 19,
        PARAM_PRESELECTOR = 23,
        PARAM_FWS_GNSS,
        RADIAT_OFF_PRESELECTOR_ON,
        FULL_STATUS,
        SET_RECEIVING_SWITCHES,
        SET_TRANSMISSION_SWITCHES,
        RADIAT_ON = 3,
        RADIAT_OFF = 10,
        REQUEST_STATE = 1,
        PARAM_FWS = 4,
        DURAT_PARAM_FWS = 5,
        PARAM_FHSS = 11,
        REQUEST_VOLTAGE = 6,
        REQUEST_POWER = 7,
        REQUEST_CURRENT = 8,
        REQUEST_TEMP = 9,
        TYPE_LOAD = 2,
        RESET = 15,
        STOP_FHSS = 12,
        ESET_APP = 16,
        ANTENNA_STATE = 30
    }
    #endregion
}
