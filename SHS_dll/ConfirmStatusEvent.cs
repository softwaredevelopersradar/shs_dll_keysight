﻿using System;

namespace SHS_DLL
{
    public class ConfirmStatusEventArgs : EventArgs
    {
        public byte CodeError { get; private set; }
        public TParamAmp[] ParamAmp { get; private set; }

        public ConfirmStatusEventArgs(byte CodeError, TParamAmp[] ParamAmp)
        {
            this.CodeError = CodeError;
            this.ParamAmp = ParamAmp;
        }
    }
}
