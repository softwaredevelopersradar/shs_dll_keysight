﻿using System;

namespace SHS_DLL
{
    public class ConfirmSetSwitch : EventArgs
    {
        public AmpCodes AmpCode { get; private set; }
        public byte[] SectorNumber { get; private set; }

        public ConfirmSetSwitch(byte[] SectorNumber, AmpCodes AmpCode)
        {
            this.SectorNumber = SectorNumber;
            this.AmpCode = AmpCode;
        }
    }
}
