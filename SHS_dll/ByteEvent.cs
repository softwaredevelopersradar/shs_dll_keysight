﻿using System;

namespace SHS_DLL
{
    public class ByteEventArgs : EventArgs
    {
        public byte[] Byte { get; private set; }

        public ByteEventArgs(byte[] Byte)
        {
            this.Byte = Byte;
        }
    }
}
