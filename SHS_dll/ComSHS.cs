﻿using Func_DLL;
using System;
using System.Collections;
using System.Linq;

namespace SHS_DLL
{
    public class ComSHS : BaseCom, IComSHS
    {
        #region Private
        public byte _fpsAddressReceiver = 0x02;
        private byte _addressSender;
        private byte _addressReceiver;
        private byte _counter;
        #endregion

        #region Event`s
        public event EventHandler<ConfirmSetEventArgs> OnConfirmSet;
        public event EventHandler<ConfirmStatusEventArgs> OnConfirmStatus;
        public event EventHandler<ConfirmStatusEventArgs> OnConfirmStatusAntenna;
        public event EventHandler<ConfirmFullStatusEventArgs> OnConfirmFullStatus;
        public event EventHandler<ConfirmSetSwitch> OnConfirmSwitch;
        public event EventHandler<byte> OnConfirmSetParam;
        public event EventHandler<ConfirmGetParamEventArgs[]> OnConfirmGetParam;
        public event EventHandler<byte> OnConfirmSaveParam;
        public event EventHandler<byte> OnRadiatOff;
        public event EventHandler<byte> OnStatus;
        public event EventHandler<byte> OnSetIRI;
        public event EventHandler<byte> OnTestGNSS;
        public event EventHandler<byte> OnSwitchPosition;
        public event EventHandler<byte> OnParamNaVi;
        public event EventHandler<byte> OnVersion;
        #endregion

        #region Constr, SetServicePart, EventOnReadByte 
        public ComSHS(byte AddressAWP, byte AddressSHS)
        {
            _addressSender = AddressAWP;
            _addressReceiver = AddressSHS;
            OnReadByte += ComSHS_OnReadByte;
        }

        private void ComSHS_OnReadByte(object sender, ByteEventArgs e)
        {
            byte[] bBufSave = new byte[LEN_ARRAY];
            int iTempLength = 0;
            bool blExistCmd = true;

            try
            {
                if (e.Byte[1] == 0x01)
                {
                    DecodeFPSCommand(e.Byte);
                    return;
                }
                else if (e.Byte[0] == 0x05 && e.Byte[1] == 0x04 && e.Byte[2] == 0x47)
                {
                    Array.Resize(ref bBufSave, e.Byte.Length - 5);
                    Array.Copy(e.Byte, 5, bBufSave, iTempLength, e.Byte.Length - 5);
                    DecodeShortFPSCommand(bBufSave);
                    return;
                }
                else if (e.Byte[0] != 0x04 && e.Byte[0] != 0x05)
                {
                    //For UU
                    DecodeShortFPSCommand(e.Byte);
                    return;
                }


                Array.Resize(ref bBufSave, iTempLength + e.Byte.Length);
                Array.Copy(e.Byte, 0, bBufSave, iTempLength, e.Byte.Length);

                iTempLength += e.Byte.Length;

                blExistCmd = true;

                while (iTempLength >= LEN_HEAD && blExistCmd)
                {
                    if (e.Byte[0] == 5 && e.Byte[1] == 4)
                    {
                        byte[] bBufHead = new byte[LEN_HEAD];
                        Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                        TServicePartSHS tServicePart = new TServicePartSHS();
                        object obj = tServicePart;
                        StrArr.ByteArrayToStructure(bBufHead, ref obj);
                        tServicePart = (TServicePartSHS)obj;

                        int iLengthCmd = tServicePart.Length;
                        if (iTempLength - LEN_HEAD >= iLengthCmd)
                        {
                            byte[] bBufDecode = new byte[iLengthCmd];
                            Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                            //For Groza-S
                            DecodeCommand(tServicePart, bBufDecode);

                            Array.Reverse(bBufSave);
                            Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                            Array.Reverse(bBufSave);

                            iTempLength -= (LEN_HEAD + iLengthCmd);

                            blExistCmd = true;

                        }
                        else
                            blExistCmd = false;
                    }
                }
            }
            catch { }
        }

        private byte[] SetShortFPSServicePart(byte bCode, int iLength)
        {
            byte[] bHead = null;

            FPSServicePartSHS tServicePart = new FPSServicePartSHS();
            if (_fpsAddressReceiver == 4)
                tServicePart.AddrSendReceiv = 0x14;
            else if (_fpsAddressReceiver == 8)
                tServicePart.AddrSendReceiv = 0x17;
            else if (_fpsAddressReceiver == 9)
                tServicePart.AddrSendReceiv = 0x18;
            else if (_fpsAddressReceiver == 10)
                tServicePart.AddrSendReceiv = 0x19;
            else
                tServicePart.AddrSendReceiv = 0x12;
            tServicePart.Code = bCode;
            tServicePart.Length = (byte)(iLength - 1);

            bHead = StrArr.StructToByteArray(tServicePart);

            return bHead;
        }

        private byte[] SetFPSServicePart(byte bCode, int iLength)
        {
            byte[] bHead = null;

            FPSServicePartSHS tServicePart = new FPSServicePartSHS();
            if (_fpsAddressReceiver == 4)
                tServicePart.AddrSendReceiv = 0x14;
            else if (_fpsAddressReceiver == 8)
                tServicePart.AddrSendReceiv = 0x17;
            else if (_fpsAddressReceiver == 9)
                tServicePart.AddrSendReceiv = 0x18;
            else if (_fpsAddressReceiver == 10)
                tServicePart.AddrSendReceiv = 0x19;
            else
                tServicePart.AddrSendReceiv = 0x12;
            tServicePart.Code = bCode;
            if (_counter == 255)
                _counter = 0;
            _counter++;
            tServicePart.Length = (byte)(iLength - 1);

            bHead = StrArr.StructToByteArray(tServicePart);

            byte[] head = new byte[] { 0x04, 0x05, 0x47, _counter, (byte)(iLength + 3) };

            var bSend = head.Concat(bHead).ToArray();

            return bSend;
        }

        private byte[] SetServicePart(byte bCode, int iLength)
        {
            byte[] bHead = null;

            TServicePartSHS tServicePart = new TServicePartSHS();
            tServicePart.AddrSend = _addressSender;
            tServicePart.AddrReceive = _addressReceiver;
            tServicePart.Code = bCode;
            if (_counter == 255)
                _counter = 0;
            tServicePart.Counter = _counter++;
            tServicePart.Length = (byte)iLength;

            bHead = StrArr.StructToByteArray(tServicePart);

            return bHead;
        }
        #endregion

        #region SendCommand
        /// <summary>
        /// Выключить излучение 
        /// </summary>
        public bool SendRadiatOFF(byte channel, bool UU)
        {
            var bData = new byte[2];
            bData[0] = channel;

            return ForFPSArray(FPSCodes.RADIAT_OFF, bData, UU);
        }

        /// <summary>
        /// Установить параметр усилителя в канале
        /// </summary>
        public bool SendParam(FPSParam[] fParam, bool UU)
        {
            try
            {
                if (fParam == null)
                {
                    return false;
                }

                var bData = new byte[fParam.Length * 9 + 1];

                for (int i = 0; i < fParam.Length; i++)
                {
                    bData[i * 9] = fParam[i].Id;
                    Array.Copy(BitConverter.GetBytes(fParam[i].FreqL), 0, bData, i * 9 + 1, 3);
                    Array.Copy(BitConverter.GetBytes(fParam[i].FreqH), 0, bData, i * 9 + 4, 3);
                    Array.Copy(BitConverter.GetBytes(fParam[i].KL), 0, bData, i * 9 + 7, 1);
                    Array.Copy(BitConverter.GetBytes(fParam[i].KH), 0, bData, i * 9 + 8, 1);
                }

                return ForFPSArray(FPSCodes.SET_PARAM_AMPL, bData, UU);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Запросить Статус устройства
        /// </summary>
        public bool SendDeviceStatus(bool UU)
        {
            var bData = new byte[1];
            bData[0] = 0;

            return ForFPSArray(FPSCodes.STATUS, bData, UU);
        }

        /// <summary>
        /// Установить тестовый сигнал для GNSS без модуляции
        /// </summary>
        public bool SendSetTestGNSS(bool UU)
        {
            var bData = new byte[1];
            bData[0] = 0;

            return ForFPSArray(FPSCodes.TEST_GNSS, bData, UU);
        }

        /// <summary>
        /// Установить положение коммутаторов 
        /// </summary>
        public bool SendPositionSwitches(bool UU, byte numberSwitches, byte switchPosition)
        {
            var bData = new byte[3];
            bData[0] = numberSwitches;
            bData[1] = switchPosition;
            bData[2] = 0;

            return ForFPSArray(FPSCodes.SWITCH_POSITION, bData, UU);
        }

        /// <summary>
        /// Установить параметры помехи для навигации и включить излучение 
        /// </summary>
        public bool SendSetParamNaViRadOn(bool UU, byte supressedNaVi)
        {
            var bData = new byte[2];
            bData[0] = supressedNaVi;
            bData[1] = 0;

            return ForFPSArray(FPSCodes.PARAM_NAVI_RADIAT_ON, bData, UU);
        }

        /// <summary>
        /// Установить параметры помехи ИРИ и включить излучение
        /// </summary>
        public bool SendSetParamsIRI(TParamFWS[] tParamFWS, bool UU)
        {
            if (tParamFWS == null)
            {
                return false;
            }

            var bData = new byte[2 + tParamFWS.Length * 7];

            bData[0] = (byte)tParamFWS.Length;

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                Array.Copy(BitConverter.GetBytes(tParamFWS[i].Freq), 0, bData, i * 7 + 1, 3);

                bData[i * 7 + 4] = tParamFWS[i].Modulation;
                bData[i * 7 + 5] = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
                bData[i * 7 + 6] = tParamFWS[i].Manipulation;
                bData[i * 7 + 7] = tParamFWS[i].Duration;
            }

            return ForFPSArray(FPSCodes.PARAM_IRI_RADIAT_ON, bData, UU);
        }

        /// <summary>
        /// Запросить параметр усилителя в канале
        /// </summary>
        public bool SendGetParam(byte[] id, bool UU)
        { 
            if (id == null)
            {
                return false;
            }

            var bData = new byte[id.Length + 1];

            for (int i = 0; i < id.Length; i++)
            {
                bData[i] = id[i];
            }

            return ForFPSArray(FPSCodes.GET_PARAM_AMPL, bData, UU);
        }

        /// <summary>
        /// Запросить версию ПО
        /// </summary>
        public bool SendGetVersion(bool UU)
        {
            var bData = new byte[1];
            bData[0] = 0;

            return ForFPSArray(FPSCodes.GET_PO_VERSION, bData, UU);
        }

        /// <summary>
        /// Сохранить в параметры усилителя в канале
        /// </summary>
        public bool SendSaveParam(bool UU)
        {
            var bData = new byte[1];
            bData[0] = 0;

            return ForFPSArray(FPSCodes.SAVE_PARAM_AMPL, bData, UU);
        }

        /// <summary>
        /// Состояние литеры
        /// </summary>
        public bool SendStatus(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.STATUS_APP, bData));
        }

        /// <summary>
        /// Состояние литеры (с учетом антенны)
        /// </summary>
        /// <param name="letter">Литера, 0 - все литеры</param>
        /// <returns></returns>
        public bool SendStatusAntennaState(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.ANTENNA_STATE, bData));
        }

        /// <summary>
        /// Настройка приёмных коммутаторов
        /// </summary>
        /// <param name="sectorsNumber">Содержит два байта, возможные значения для каждого 1..6 – номер сектора; 7 – ненапрвленная антенна( 2,4 )</param>
        /// <returns>Результат</returns>
        public bool SendSetReceivingSwitches(byte[] sectorsNumber)
        {
            var bData = new byte[2];
            bData[0] = sectorsNumber[0];
            bData[1] = sectorsNumber[1];

            return WriteData(FormArrayForSend(AmpCodes.SET_RECEIVING_SWITCHES, bData));
        }

        /// <summary>
        /// Настройка приёмных коммутаторов
        /// </summary>
        /// <param name="sectorsNumber">Содержит два байта, возможные значения для каждого 1..6 – номер сектора; 7 – ненапрвленная антенна( 2,4 )</param>
        /// <returns>Результат</returns>
        public bool SendSetTransmissionSwitches(byte[] sectorsNumber)
        {
            var bData = new byte[2];
            bData[0] = sectorsNumber[0];
            bData[1] = sectorsNumber[1];

            return WriteData(FormArrayForSend(AmpCodes.SET_TRANSMISSION_SWITCHES, bData));
        }

        /// <summary>
        /// Выключить излучение
        /// </summary>
        public bool SendRadiatOff(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.RADIAT_OFF_APP, bData));
        }


        /// <summary>
        /// Отправляет запрос статуса на формирователь и ресетит усилители
        /// </summary>
        public bool SendReset(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.RESET_APP, bData));
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        /// </summary>
        public bool SendSetParamFWS(int iDuration, TParamFWS[] tParamFWS)
        {
            if (tParamFWS == null)
            {
                return false;
            }

            var bData = new byte[3 + tParamFWS.Length * 7];

            Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 3);

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                Array.Copy(BitConverter.GetBytes(tParamFWS[i].Freq), 0, bData, i * 7 + 3, 3);

                bData[i * 7 + 6] = tParamFWS[i].Modulation;
                bData[i * 7 + 7] = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
                bData[i * 7 + 8] = tParamFWS[i].Manipulation;
                bData[i * 7 + 9] = tParamFWS[i].Duration;
            }

            return WriteData(FormArrayForSend(AmpCodes.PARAM_FWS_APP, bData));
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности c мощностью
        /// </summary>
        public bool SendSetParamFWS(int iDuration, TParamFWS[] tParamFWS, byte[] Power)
        {
            if (tParamFWS == null)
            {
                return false;
            }

            var bData = new byte[3 + tParamFWS.Length * 8];

            Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 3);

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                Array.Copy(BitConverter.GetBytes(tParamFWS[i].Freq), 0, bData, i * 8 + 3, 3);

                bData[i * 8 + 6] = tParamFWS[i].Modulation;
                bData[i * 8 + 7] = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
                bData[i * 8 + 8] = tParamFWS[i].Manipulation;
                bData[i * 8 + 9] = tParamFWS[i].Duration;
                bData[i * 8 + 10] = Power[i];
            }

            return WriteData(FormArrayForSend(AmpCodes.PARAM_FWS_APP, bData));
        }


        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение
        /// </summary>
        public bool SendSetGNSS(TGpsGlonass gnss)
        {
            var bData = new byte[4];

            bData[0] = Convert.ToByte(gnss.gpsL1);
            bData[1] = Convert.ToByte(gnss.gpsL2);
            bData[2] = Convert.ToByte(gnss.glnssL1);
            bData[3] = Convert.ToByte(gnss.glnssL2);

            return WriteData(FormArrayForSend(AmpCodes.GNSS_APP, bData));
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение 2
        /// </summary>
        public bool SendSetGNSS(TGpsGlonass gpsGlonass, TBeidouGalileo beidouGalileo)
        {
            var bData = new byte[8];

            bData[0] = Convert.ToByte(gpsGlonass.gpsL1);
            bData[1] = Convert.ToByte(gpsGlonass.gpsL2);
            bData[2] = Convert.ToByte(gpsGlonass.glnssL1);
            bData[3] = Convert.ToByte(gpsGlonass.glnssL2);
            bData[4] = Convert.ToByte(beidouGalileo.beidouL1);
            bData[5] = Convert.ToByte(beidouGalileo.beidouL2);
            bData[6] = Convert.ToByte(beidouGalileo.galileoL1);
            bData[7] = Convert.ToByte(beidouGalileo.galileoL2);

            return WriteData(FormArrayForSend(AmpCodes.NAVI_RADIAT_ON, bData));
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение с мощностью
        /// </summary>
        public bool SendSetGNSS(TGpsGlonass gnss, byte Power)
        {
            var bData = new byte[5];

            bData[0] = Convert.ToByte(gnss.gpsL1);
            bData[1] = Convert.ToByte(gnss.gpsL2);
            bData[2] = Convert.ToByte(gnss.glnssL1);
            bData[3] = Convert.ToByte(gnss.glnssL2);
            bData[4] = Power;

            return WriteData(FormArrayForSend(AmpCodes.GNSS_APP, bData));
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение 2 с мощностью
        /// </summary>
        public bool SendSetGNSS(TGpsGlonass gpsGlonass, TBeidouGalileo beidouGalileo, byte Power)
        {
            var bData = new byte[9];

            bData[0] = Convert.ToByte(gpsGlonass.gpsL1);
            bData[1] = Convert.ToByte(gpsGlonass.gpsL2);
            bData[2] = Convert.ToByte(gpsGlonass.glnssL1);
            bData[3] = Convert.ToByte(gpsGlonass.glnssL2);
            bData[4] = Convert.ToByte(beidouGalileo.beidouL1);
            bData[5] = Convert.ToByte(beidouGalileo.beidouL2);
            bData[6] = Convert.ToByte(beidouGalileo.galileoL1);
            bData[7] = Convert.ToByte(beidouGalileo.galileoL2);
            bData[8] = Power;

            return WriteData(FormArrayForSend(AmpCodes.NAVI_RADIAT_ON, bData));
        }

        /// <summary>
        /// Включить спуфинг
        /// </summary>
        public bool SendSetSPOOF()
        {
            var bData = new byte[0];

            return WriteData(FormArrayForSend(AmpCodes.SPOOF_APP, bData));
        }

        /// <summary>
        /// Включить спуфинг с мощностью
        /// </summary>
        public bool SendSetSPOOF(byte Power)
        {
            var bData = new byte[1];

            bData[0] = Power;

            return WriteData(FormArrayForSend(AmpCodes.SPOOF_APP, bData));
        }


        /// <summary>
        /// Настройки преселектора
        /// </summary>
        public bool SendParamPreselector(bool[] amplifires)
        {
            var data = new byte[2];
            var myBitArray = new BitArray(16, false);
            for (int i = 0; i < 10; i++)
            {
                myBitArray[i] = amplifires[i];
            }
            myBitArray.CopyTo(data, 0);
            return WriteData(FormArrayForSend(AmpCodes.PARAM_PRESELECTOR, data));
        }


        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и системы навигации. Включить излучение заданной длительности
        /// </summary>
        public bool SendParamFwsGnss(int iDuration, TGpsGlonass gnss, TBeidouGalileo beidouGalileo, TParamFWS[] tParamFWS, byte[] power, byte powerAll)
        {
            var bData = new byte[5 + tParamFWS.Length * 8];

            var myBitArray = new BitArray(8, false);

            myBitArray[0] = gnss.gpsL1;
            myBitArray[1] = gnss.gpsL2;
            myBitArray[2] = gnss.glnssL1;
            myBitArray[3] = gnss.glnssL2;
            myBitArray[4] = beidouGalileo.beidouL1;
            myBitArray[5] = beidouGalileo.beidouL2;
            myBitArray[6] = beidouGalileo.galileoL1;
            myBitArray[7] = beidouGalileo.galileoL2;

            myBitArray.CopyTo(bData, 3);
            Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 3);

            bData[4] = powerAll;

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                Array.Copy(BitConverter.GetBytes(tParamFWS[i].Freq), 0, bData, i * 8 + 5, 3);
                bData[i * 8 + 8] = tParamFWS[i].Modulation;
                bData[i * 8 + 9] = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
                bData[i * 8 + 10] = tParamFWS[i].Manipulation;
                bData[i * 8 + 11] = tParamFWS[i].Duration;
                bData[i * 8 + 12] = power[i];
            }

            return WriteData(FormArrayForSend(AmpCodes.PARAM_FWS_GNSS, bData));
        }


        /// <summary>
        /// Выключить излучение и включить преселектор
        /// </summary>
        public bool SendPreselectorOn(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.RADIAT_OFF_PRESELECTOR_ON, bData));
        }

        /// <summary>
        /// Переключение реле
        /// </summary>
        public bool SendRelaySwitching(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.RELAY_SWITCHING, bData));
        }

        /// <summary>
        /// Полный статус
        /// </summary>
        public bool SendFullStatus(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.FULL_STATUS, bData));
        }
        #endregion

        #region CreateArrayForSend
        private byte[] FormArrayForSend(AmpCodes code, byte[] data)
        {
            // create head array
            byte[] head = SetServicePart((byte)code, data.Length);
            // create send array
            var bSend = head.Concat(data).ToArray();

            return bSend;
        }

        //Для FPS
        private byte[] FormArrayForSend(FPSCodes code, byte[] data, bool UU)
        {
            if (UU)
            {
                // create head array
                byte[] head = SetFPSServicePart((byte)code, data.Length);
                // create send array
                var bSend = head.Concat(data).ToArray();

                return bSend;
            }
            else
            {
                // create head array
                byte[] head = SetShortFPSServicePart((byte)code, data.Length);
                // create send array
                var bSend = head.Concat(data).ToArray();

                return bSend;
            }
        }

        private bool ForFPSArray(FPSCodes code, byte[] bData, bool UU)
        {
            uint CRC = new uint();

            if (UU)
            {
                var byteArray = FormArrayForSend(code, bData, UU);

                for (int i = 5; i < byteArray.Length; i++)
                {
                    CRC += byteArray[i];
                }

                byteArray[byteArray.Length - 1] = Convert.ToByte(CRC % 255);

                return WriteData(byteArray);
            }
            else
            {
                var byteArray = FormArrayForSend(code, bData, UU);

                foreach (var a in byteArray)
                {
                    CRC += a;
                }

                byteArray[byteArray.Length - 1] = Convert.ToByte(CRC % 255);

                return WriteData(byteArray);
            }
        }
        #endregion

        #region ReceiveFunction

        private void DecodeShortFPSCommand(byte[] bData)
        { 
            switch (bData[1])
            {
                case (byte)FPSCodes.SET_PARAM_AMPL:

                    OnConfirmSetParam?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.GET_PARAM_AMPL:

                    ConfirmGetParamEventArgs[] data = new ConfirmGetParamEventArgs[(bData.Length - 4) / 6];

                    for (int i = 3; i < bData.Length - 4; i += 10)
                    {
                        for (int j = 0; j < data.Length; j++)
                        {
                            data[j] = new ConfirmGetParamEventArgs(bData[i], BitConverter.ToUInt16(bData, i + 1), BitConverter.ToUInt16(bData, i + 4), bData[i + 7], bData[i + 8], Convert.ToBoolean(bData[i + 9]));
                        }
                    }
                    OnConfirmGetParam?.Invoke(this, data);
                    break;

                case (byte)FPSCodes.SAVE_PARAM_AMPL:

                    OnConfirmSaveParam?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.RADIAT_OFF:
                    OnRadiatOff?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.PARAM_IRI_RADIAT_ON:
                    OnSetIRI?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.TEST_GNSS:
                    OnTestGNSS?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.SWITCH_POSITION:
                    OnSwitchPosition?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.PARAM_NAVI_RADIAT_ON:
                    OnParamNaVi?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.STATUS:
                    OnStatus?.Invoke(this, bData[3]);
                    break;

                case (byte)FPSCodes.GET_PO_VERSION:
                    OnVersion?.Invoke(this, bData[3]);
                    break;
            }
        }

        private void DecodeFPSCommand(byte[] bData)
        { 
            switch (bData[2])
            {
                case (byte)FPSCodes.SET_PARAM_AMPL:

                    OnConfirmSetParam?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.GET_PARAM_AMPL:

                    ConfirmGetParamEventArgs[] data = new ConfirmGetParamEventArgs[(bData.Length - 6) / 6];

                    for (int i = 5; i < bData.Length - 4; i += 10)
                    {
                        for (int j = 0; j < data.Length; j++)
                        {
                            data[j] = new ConfirmGetParamEventArgs(bData[i], BitConverter.ToUInt16(bData, i + 1), BitConverter.ToUInt16(bData, i + 4), bData[i + 7], bData[i + 8], Convert.ToBoolean(bData[i + 9]));
                        }
                    }
                    OnConfirmGetParam?.Invoke(this, data);
                    break;

                case (byte)FPSCodes.SAVE_PARAM_AMPL:

                    OnConfirmSaveParam?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.RADIAT_OFF:
                    OnRadiatOff?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.PARAM_IRI_RADIAT_ON:
                    OnSetIRI?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.TEST_GNSS:
                    OnTestGNSS?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.SWITCH_POSITION:
                    OnSwitchPosition?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.PARAM_NAVI_RADIAT_ON:
                    OnParamNaVi?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.STATUS:
                    OnStatus?.Invoke(this, bData[5]);
                    break;

                case (byte)FPSCodes.GET_PO_VERSION:
                    OnVersion?.Invoke(this, bData[5]);
                    break;
            }
        }

        private void DecodeCommand(TServicePartSHS tServicePart, byte[] bData)
        { 
            if (tServicePart.AddrReceive != _addressSender)
                return;
            if (bData == null)
                return;

            byte bCodeError = bData[0];

            switch (tServicePart.Code)
            {
                case (byte)FPSCodes.SET_PARAM_AMPL:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.PARAM_FWS_APP));
                    break;

                case (byte)FPSCodes.GET_PARAM_AMPL:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.PARAM_FWS_APP));
                    break;

                case (byte)FPSCodes.SAVE_PARAM_AMPL:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.PARAM_FWS_APP));
                    break;

                case (byte)AmpCodes.PARAM_FWS_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.PARAM_FWS_APP));
                    break;

                case (byte)AmpCodes.RADIAT_OFF_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.RADIAT_OFF_APP));
                    break;

                case (byte)AmpCodes.RESET_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.RESET_APP));
                    break;

                case (byte)AmpCodes.SPOOF_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.SPOOF_APP));
                    break;

                case (byte)AmpCodes.GNSS_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.GNSS_APP));
                    break;

                case (byte)AmpCodes.NAVI_RADIAT_ON:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.NAVI_RADIAT_ON));
                    break;

                case (byte)AmpCodes.RELAY_SWITCHING:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.RELAY_SWITCHING));
                    break;

                case (byte)AmpCodes.SET_RECEIVING_SWITCHES:

                    OnConfirmSwitch?.Invoke(this, new ConfirmSetSwitch(bData, AmpCodes.SET_RECEIVING_SWITCHES));
                    break;

                case (byte)AmpCodes.SET_TRANSMISSION_SWITCHES:

                    OnConfirmSwitch?.Invoke(this, new ConfirmSetSwitch(bData, AmpCodes.SET_TRANSMISSION_SWITCHES));
                    break;

                case (byte)AmpCodes.STATUS_APP:
                    {
                        int length = bData.Length;
                        if (length != 31)
                            return;

                        var tParamAmp = new TParamAmp[(length - 1) / 6];

                        for (int i = 0; i < (length - 1) / 6; i++)
                        {
                            tParamAmp[i] = new TParamAmp();

                            tParamAmp[i].Snt = bData[i * 6 + 1];
                            tParamAmp[i].Rad = bData[i * 6 + 2];

                            unchecked
                            {
                                tParamAmp[i].Temp = (sbyte)(bData[i * 6 + 3]);
                            }

                            tParamAmp[i].Current = Convert.ToByte(bData[i * 6 + 4]);
                            tParamAmp[i].Power = bData[i * 6 + 5];
                            tParamAmp[i].Error = bData[i * 6 + 6];
                        }

                        OnConfirmStatus?.Invoke(this, new ConfirmStatusEventArgs(bCodeError, tParamAmp));
                        break;
                    }

                case (byte)AmpCodes.ANTENNA_STATE:
                    {
                        int length = bData.Length;
                        if (length != 36)
                            return;

                        var tParamAmp = new TParamAmp[(length - 1) / 7];

                        for (int i = 0; i < (length - 1) / 7; i++)
                        {
                            tParamAmp[i] = new TParamAmp();

                            tParamAmp[i].Snt = bData[i * 7 + 1];
                            tParamAmp[i].Rad = bData[i * 7 + 2];

                            unchecked
                            {
                                tParamAmp[i].Temp = (sbyte)(bData[i * 7 + 3]);
                            }

                            tParamAmp[i].Current = Convert.ToByte(bData[i * 7 + 4]);
                            tParamAmp[i].Power = bData[i * 7 + 5];
                            tParamAmp[i].Error = bData[i * 7 + 6];
                            tParamAmp[i].Antenna = bData[i * 7 + 7];
                        }

                        OnConfirmStatusAntenna?.Invoke(this, new ConfirmStatusEventArgs(bCodeError, tParamAmp));
                        break;
                    }
                case (byte)AmpCodes.PARAM_PRESELECTOR:

                    if (bData.Length != 34)
                        return;
                    GetFullStatus(bData, AmpCodes.PARAM_PRESELECTOR);
                    break;

                case (byte)AmpCodes.PARAM_FWS_GNSS:

                    if (bData.Length != 34)
                        return;
                    GetFullStatus(bData, AmpCodes.PARAM_FWS_GNSS);
                    break;

                case (byte)AmpCodes.RADIAT_OFF_PRESELECTOR_ON:

                    if (bData.Length != 34)
                        return;
                    GetFullStatus(bData, AmpCodes.RADIAT_OFF_PRESELECTOR_ON);
                    break;

                case (byte)AmpCodes.FULL_STATUS:

                    if (bData.Length != 34)
                        return;

                    GetFullStatus(bData, AmpCodes.FULL_STATUS);
                    break;

                default:
                    break;
            }

        }

        private void GetFullStatus(byte[] bData, AmpCodes AmpCode)
        {
            byte bCodeError = bData[0];
            bool relay = Convert.ToBoolean(bData[3]);

            byte[] amp = new byte[2];
            Array.Copy(bData, 1, amp, 0, 2);
            var myBitAray = new BitArray(amp);
            var amplifiers = new bool[10];
            for (int i = 0; i < amplifiers.Length; i++)
            {
                amplifiers[i] = myBitAray[i];
            }

            var paramAmp = new TParamAmp[5];
            for (int i = 0; i < 5; i++)
            {
                paramAmp[i] = new TParamAmp()
                {
                    Snt = bData[i * 6 + 4],
                    Rad = bData[i * 6 + 5],
                    Current = Convert.ToByte(bData[i * 6 + 7]),
                    Power = bData[i * 6 + 8],
                    Error = bData[i * 6 + 9]
                };

                unchecked
                {
                    paramAmp[i].Temp = (sbyte)(bData[i * 6 + 6]);
                }
            }

            OnConfirmFullStatus?.Invoke(this, new ConfirmFullStatusEventArgs(bCodeError, amplifiers, relay, paramAmp, AmpCode));
        }
        #endregion

        #region CreateDev

        private byte CreateDev(byte Modulation, int Dev)
        {
            if (Modulation == 0 || Modulation == 1)
            {
                return 0;
            }
            else if (Modulation == 4)
            {
                if (Dev >= 1000 && Dev <= 127000)
                {
                    Dev /= 1000;
                    return DevConverter(Dev, true);
                }
                else if (Dev < 1000)
                {
                    Dev /= 10;
                    return DevConverter(Dev, false);
                }
                return 0;
            }
            else if (Modulation == 5)
            {
                if (Dev >= 1000 && Dev <= 255000)
                {
                    Dev /= 1000;
                    return (byte)Dev;
                }
                else if (Dev < 1000)
                {
                    Dev /= 10;
                    return (byte)Dev;
                }
                return 0;
            }
            return 0;
        }

        private byte DevConverter(int Dev, bool highBit)
        {
            string s = Convert.ToString(Dev, 2); //Convert to binary in a string

            int[] bits = s.PadLeft(8, '0') // Add 0's from left
                         .Select(c => int.Parse(c.ToString())) // convert each char to int
                         .ToArray(); // Convert IEnumerable from select to Array
            BitArray b = new BitArray(8, false);
            for (int i = 1; i < 8; i++)
            {
                b[i] = Convert.ToBoolean(bits[i]);
            }
            b[0] = highBit;
            return ConvertToByte(b);
        }

        private byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            var reversed = new BitArray(bits.Cast<bool>().Reverse().ToArray());
            reversed.CopyTo(bytes, 0);
            return bytes[0];
        }
        #endregion

    }
}
