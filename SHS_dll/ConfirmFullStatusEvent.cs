﻿using System;

namespace SHS_DLL
{
    public class ConfirmFullStatusEventArgs : EventArgs
    {
        public byte CodeError { get; private set; }
        public bool[] Amplifiers { get; private set; }
        public bool Relay { get; private set; }
        public TParamAmp[] ParamAmp { get; private set; }
        public AmpCodes Amp { get; private set; }

        public ConfirmFullStatusEventArgs(byte CodeError, bool[] Amplifiers, bool Relay, TParamAmp[] ParamAmp, AmpCodes Amp)
        {
            this.CodeError = CodeError;
            this.Amplifiers = Amplifiers;
            this.Relay = Relay;
            this.ParamAmp = ParamAmp;
            this.Amp = Amp;
        }
    }

    public class ConfirmGetParamEventArgs : EventArgs
    {
        public byte Id { get; set; }
        public int Fl { get; set; }
        public int Fh { get; set; }
        public byte Kl { get; set; }
        public byte Kh { get; set; }
        public bool Status { get; set; }

        public ConfirmGetParamEventArgs(byte Id, int Fl, int Fh, byte Kl, byte Kh, bool Status)
        {
            this.Id = Id;
            this.Fl = Fl;
            this.Fh = Fh;
            this.Kl = Kl;
            this.Kh = Kh;
            this.Status = Status;
        }
    }

}
