﻿using System;

namespace SHS_DLL
{
    public class ConfirmSetEventArgs : EventArgs
    {
        public byte CodeError { get; private set; }
        public AmpCodes AmpCode { get; private set; }

        public ConfirmSetEventArgs(byte CodeError, AmpCodes AmpCode)
        {
            this.CodeError = CodeError;
            this.AmpCode = AmpCode;
        }
    }
}
