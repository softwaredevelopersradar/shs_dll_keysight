﻿using System;
using System.IO.Ports;

namespace SHS_DLL
{
    interface IComSHS
    {
        bool OpenPort(string portName, Int32 baudRate, Parity parity, Int32 dataBits, StopBits stopBits);
        bool ClosePort();

        bool SendStatus(byte letter);
        bool SendRadiatOff(byte letter);
        bool SendReset(byte letter);
        bool SendSetParamFWS(int iDuration, TParamFWS[] tParamFWS);
        bool SendSetParamFWS(int iDuration, TParamFWS[] tParamFWS, byte[] Power);
        bool SendSetGNSS(TGpsGlonass gnss);
        bool SendSetGNSS(TGpsGlonass gnss, byte Power);
        bool SendSetSPOOF();
        bool SendSetSPOOF(byte Power);
        bool SendParamPreselector(bool[] amplifires);
        bool SendParamFwsGnss(int iDuration, TGpsGlonass gnss, TBeidouGalileo beidouGalileo, TParamFWS[] tParamFWS, byte[] power, byte powerAll);
        bool SendPreselectorOn(byte letter);
        bool SendFullStatus(byte letter);


        event EventHandler<ByteEventArgs> OnReadByte;
        event EventHandler<ByteEventArgs> OnWriteByte;
        event EventHandler<ConfirmSetEventArgs> OnConfirmSet;
        event EventHandler<ConfirmStatusEventArgs> OnConfirmStatus;
        event EventHandler<ConfirmFullStatusEventArgs> OnConfirmFullStatus;
    }
}
